<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 9/1/2017
 * Time: 11:38 AM
 */

class DeviceManager extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'users');
        $this->load->model('DeviceManager_model', 'devices');
    }

    public function index_get()
    {
        $devices = array(
            'devices' => $this->devices->get_all(),
        );
        $arr_return['data'] = array(
            'status' => 200,
            'message' => "Success",
            'info' => $devices
        );
        // Display all books
        $this->response([
            $arr_return
        ]);
    }

    public function store_post()
    {
        //var_dump($_POST);
        // Create a new book
        $betterystate = $this->post('betterystate'); // POST param
        $message = $this->post('message'); // POST param
        $ssid = $this->post('ssid'); // POST param
        $wifilevel = $this->post('wifilevel'); // POST param
        $wifimessage = $this->post('wifimessage'); // POST param
        $httpresponse = $this->post('httpresponse'); // POST param
        $ip_address = $this->post('ip_address'); // POST param
        $mac_address = $this->post('mac_address'); // POST param

        $data = array(
            'betterystate' => $betterystate,
            'message' => $message,
            'ssid' => $ssid,
            'wifilevel' => $wifilevel,
            'wifimessage' => $wifimessage,
            'httpresponse' => $httpresponse,
            'ip_address' => $ip_address,
            'mac_address' => $mac_address
        );

        $ret = $this->devices->insert($data);
        // $arr_return = array();
        if ($ret) {
            $arr_return = array(
                'status' => 200,
                'message' => "Success"
            );

        } else {
            $arr_return = array(
                'status' => 303,
                'message' => "Error occured.",
            );
        }
        $arr_return['data']=$data;
        $this->response([
            $arr_return
        ], 200);

    }


}