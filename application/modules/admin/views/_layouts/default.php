<div class="wrapper">

	<?php $this->load->view('_partials/navbar'); ?>

	<?php // Left side column. contains the logo and sidebar ?>
	<aside class="main-sidebar">
		<section class="sidebar">
			<div class="user-panel">
				<div class="pull-left info" style="left:5px">
					<p><?php echo $user->first_name; ?></p>
					<a href="panel/account"><i class="fa fa-circle text-success"></i> Online</a>
				</div>
			</div>
			<?php // (Optional) Add Search box here ?>
			<?php //$this->load->view('_partials/sidemenu_search'); ?>
			<?php $this->load->view('_partials/sidemenu'); ?>
		</section>
	</aside>


	<?php // Right side column. Contains the navbar and content of the page ?>
	<div class="content-wrapper">

		<section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-md-3"><h4 style="text-transform: uppercase"><?php echo $page_title; ?></h4></div>
                        <div class="col-md-offset-4 col-md-5"><?php $this->load->view('_partials/breadcrumb'); ?></div>
                    </div>


                </div>
                <?php $this->load->view($inner_view); ?>
                <?php $this->load->view('_partials/back_btn'); ?>
            </div>

		</section>
	</div>

	<?php // Footer ?>
	<?php $this->load->view('_partials/footer'); ?>

</div>