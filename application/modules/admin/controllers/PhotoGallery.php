<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 8/25/2017
 * Time: 2:28 PM
 */

class PhotoGallery extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_builder');
    }

    // Frontend User CRUD
    public function index()
    {
        $crud = $this->generate_crud('tb_photogallery_items');
        $crud->set_subject('Phot Gallery');
        $crud->columns('cat_id', 'name', 'location', 'img', 'enabled');
        $crud->field_type('enabled','dropdown',array('1' => 'Publish', '0' => 'Unpublished'));
        $this->unset_crud_fields();
        $crud->set_relation('cat_id','tb_photogallery_cat','name');
        $crud->display_as('name','Gallery Name');
        $crud->display_as('location','Location');
        $crud->display_as('cat_id','Galley Category');
        $crud->display_as('img','Image Upload');
        $crud->display_as('enabled','Is Publish');
        $crud->set_field_upload('img','assets/uploads/photogallery');
        $this->mPageTitle = 'Photo Gallery';
        $this->render_crud();
    }

    public function category(){
        $crud = $this->generate_crud('tb_photogallery_cat');
        $crud->set_subject('Gallery Category');
        $crud->columns('name', 'enabled');
        $crud->display_as('name', 'Category Name');
        $crud->display_as('enabled', 'Is Publish');
        $this->unset_crud_fields('sort');
        $crud->field_type('enabled', 'dropdown', array('1' => 'Publish', '0' => 'Unpublished'));
        $this->mPageTitle = 'Gallery Category';
        $this->render_crud();
    }
}