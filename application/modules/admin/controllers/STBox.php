<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 8/29/2017
 * Time: 9:02 AM
 */

class STBox extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_builder');
    }
    public function index(){
        $crud = $this->generate_crud('tb_rebootstb');
        $crud->set_subject('ST Box');
        $crud->columns('ipaddr','timestamp','enabled');
        $this->unset_crud_fields('sort');
        $crud->field_type('enabled','dropdown',array('1' => 'Publish', '0' => 'Unpublished'));
        $this->mPageTitle = 'STBox Service';
        $this->render_crud();
    }

}