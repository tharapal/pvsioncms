<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 8/29/2017
 * Time: 9:02 AM
 */

class Echeck extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_builder');
    }
    public function index(){
        $crud = $this->generate_crud('tb_main_slider');
        $crud->set_subject('E-Checking');
        $crud->columns('text', 'image', 'enabled');
        $this->unset_crud_fields('sort');
        $crud->field_type('enabled','dropdown',array('1' => 'Publish', '0' => 'Unpublished'));
        $crud->set_field_upload('image','assets/uploads/banner');
        $this->mPageTitle = 'E Checking';
        $this->render_crud();
    }

}