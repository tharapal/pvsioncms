<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 8/25/2017
 * Time: 2:28 PM
 */

class AboutUs extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_builder');
    }

    // Frontend User CRUD
    public function index()
    {
        $crud = $this->generate_crud('tb_about_us');
        $crud->set_subject('About Us');
        $crud->columns('name', 'desc_en', 'img','button_img', 'enabled');
        $crud->field_type('enabled','dropdown',array('1' => 'Publish', '0' => 'Unpublished'));
        $this->unset_crud_fields('sort');
        //$crud->display_as('name','Gallery Name');
        $crud->display_as('desc_en','Description');
        $crud->display_as('button_img','Button Image Upload');
        $crud->display_as('img','Image Upload');
        $crud->display_as('enabled','Is Publish');
        $crud->set_field_upload('button_img','assets/uploads/aboutus');
        $crud->set_field_upload('img','assets/uploads/aboutus');
        $this->mPageTitle = 'About Us';
        $this->render_crud();
    }


}