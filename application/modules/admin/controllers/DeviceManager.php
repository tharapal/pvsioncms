<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 9/1/2017
 * Time: 2:56 PM
 */

class DeviceManager extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_builder');
    }

    // Frontend User CRUD
    public function index()
    {
        $crud = $this->generate_crud('device_manager');
        $crud->set_subject('Device Manager');
        $crud->columns('betterystate', 'message', 'ssid','wifilevel', 'wifimessage','httpresponse');
       // $crud->field_type('enabled','dropdown',array('1' => 'Publish', '0' => 'Unpublished'));
        $this->unset_crud_fields('sort');
        $crud->display_as('betterystate','Battery');
        $crud->display_as('message','notification');
        $crud->display_as('ssid','Network ID');
        $crud->display_as('wifilevel','WiFi Level');
        $crud->display_as('wifimessage','Message');
        $crud->display_as('httpresponse','Internet Status');

        $this->mPageTitle = 'Device Manager';
        $this->render_crud();
    }

}