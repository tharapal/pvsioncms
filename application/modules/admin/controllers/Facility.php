<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 8/29/2017
 * Time: 9:02 AM
 */

class Facility extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_builder');
    }
    public function index(){
        $crud = $this->generate_crud('tb_facilities');
        $crud->set_subject('Facilities');
        $this->unset_crud_fields('title_ru','title_ge','desc_ru','desc_ge','hour','sort');
        $crud->columns('title_en','desc_en', 'picture', 'enabled');
        $crud->display_as('title_en','Facility Name');
        $crud->display_as('desc_en','Description');
        $crud->display_as('picture','Upload Image');
        $crud->display_as('enabled','Is Pulish');
        $crud->set_field_upload('picture','assets/uploads/facility');
        $crud->field_type('enabled','dropdown',array('1' => 'Publish', '0' => 'Unpublished'));
        $this->mPageTitle = 'Hotel Facility';
        $this->render_crud();
    }

}