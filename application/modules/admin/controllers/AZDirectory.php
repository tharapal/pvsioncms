<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 8/25/2017
 * Time: 2:28 PM
 */

class AZDirectory extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_builder');
    }

    // Frontend User CRUD
    public function index()
    {
        $crud = $this->generate_crud('tb_azdirectory_item');
        $crud->set_subject('A-Z Directory');
        $crud->columns('name', 'description', 'group_id', 'enabled');


        $crud->set_relation('group_id','tb_azdirectory_categories','name');
        $crud->field_type('enabled','dropdown',array('1' => 'Publish', '0' => 'Unpublished'));
        $this->unset_crud_fields('key_info', 'a_name_ru', 'a_name_ge', 'key_info_ru', 'key_info_ge', 'title_ru', 'title_ge', 'desc_ru', 'desc_ge', 'sort');
        $this->mPageTitle = 'A-Z Directory';
        $this->render_crud();
    }

    public function dircategory()
    {
        $crud = $this->generate_crud('tb_azdirectory_categories');
        $crud->set_subject('Directory Category');
        $crud->columns('name', 'enabled');
       // $crud->callback_add_field('enabled', array($this, 'add_field_callback_1'));
        //$crud->field_type('enabled','dropdown',array(" "=>'Please set Status','1' => 'Publish', '2' => 'Unpublished'));
        $crud->field_type('enabled','dropdown',
            array('1' => 'active', '2' => 'private','3' => 'spam' , '4' => 'deleted'));
        $this->unset_crud_fields('');
        $this->mPageTitle = 'Directory Category';
        $this->render_crud();

    }
}