<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 8/28/2017
 * Time: 5:25 PM
 */
class Banner extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_builder');
    }
    public function index(){
        $crud = $this->generate_crud('tb_main_slider');
        $crud->set_subject('Banner');
        $this->unset_crud_fields('sort');
        $crud->columns('text', 'image', 'enabled');
        $crud->field_type('enabled','dropdown',array('1' => 'Publish', '0' => 'Unpublished'));
        $crud->set_field_upload('image','assets/uploads/banner');
        $this->mPageTitle = 'Banner';
        $this->render_crud();
    }
}