<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 8/29/2017
 * Time: 9:02 AM
 */

class SignagePublic extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_builder');
    }

    public function index()
    {
        $timearray = $this->halfHourTimes();
        $crud = $this->generate_crud('tb_signage_events');
        $crud->set_subject('SignageMeeting');
        $crud->columns('event_title', 'event_location', 'event_date', 'event_time', 'event_logo', 'enabled');
        $this->unset_crud_fields('sort', 'timestamp');
        $crud->field_type('event_time', 'dropdown', $timearray);
        $crud->field_type('enabled', 'dropdown', array('1' => 'Publish', '0' => 'Unpublished'));
        $crud->set_field_upload('event_logo', 'assets/uploads/signpub/event');
        $this->mPageTitle = 'Signage Meeting Room';
        $this->render_crud();
    }

    public function promotions()
    {
        $timearray = $this->halfHourTimes();
        //var_dump($timearray);
        $crud = $this->generate_crud('tb_signage_promotion');
        $crud->set_subject('SignageMeeting Event');
        $crud->columns('name', 'img', 'enabled');
        $this->unset_crud_fields('sort', 'timestamp');
        $crud->display_as('name', 'Event Name');
        $crud->display_as('img', 'Upload Image');
        $crud->display_as('enabled', 'Is Publish');
        $crud->field_type('enabled', 'dropdown', array('1' => 'Publish', '0' => 'Unpublished'));
        $crud->set_field_upload('img', 'assets/uploads/signpub/promo');
        $this->mPageTitle = 'Signage Public Promotions';
        $this->render_crud();
    }

    public function exchangerate()
    {
        $crud = $this->generate_crud('tb_dream_signage_cur');
        $crud->set_subject('Exchange Rates');
        $crud->columns('currency', 'buy', 'sell', 'enabled');
        $this->unset_crud_fields('sort', 'timestamp');
        $crud->field_type('enabled', 'dropdown', array('1' => 'Publish', '0' => 'Unpublished'));
        $this->mPageTitle = 'Exchange Rates';
        $this->render_crud();
    }

    public function worldclock()
    {

        $crud = $this->generate_crud('tb_worldclock');
        $crud->set_subject('World Clock');
        $crud->columns('country_name', 'time_zone', 'enabled');
        $crud->display_as('enabled', 'Is Publish');
        $crud->field_type('enabled', 'dropdown', array('1' => 'Publish', '0' => 'Unpublished'));
        $this->mPageTitle = 'World Clock';
        $this->render_crud();
    }

    public function worldradio()
    {

        $crud = $this->generate_crud('tb_worldradio');
        $crud->set_subject('World Radio');
        $crud->columns('radio_name', 'radio_type', 'continent_id', 'country_id', 'radio_language', 'radio_weblink', 'radio_img', 'enabled');
        $crud->display_as('radio_name', 'Radio Name');
        $crud->display_as('radio_type', 'Radio Type');
        $crud->display_as('continent_id', 'Continent');
        $crud->display_as('country_id', 'Country');
        $crud->display_as('radio_language', 'Language');
        $crud->display_as('radio_weblink', 'Web Site');
        $crud->display_as('radio_img', 'Upload Image');
        $crud->display_as('enabled', 'Is Publish');
        $crud->field_type('continent_id', 'dropdown', array('1'=>"Continent"));
        $crud->field_type('country_id', 'dropdown', array('1'=>"Country"));
        $crud->field_type('radio_type', 'dropdown', array('1'=>"Radio"));
        $this->unset_crud_fields();
        $crud->field_type('enabled', 'dropdown', array('1' => 'Publish', '0' => 'Unpublished'));
        $crud->set_field_upload('radio_img', 'assets/uploads/signpub/radio');
        $this->mPageTitle = 'World Radio';
        $this->render_crud();
    }

    public function backgroundcat()
    {

        $crud = $this->generate_crud('tb_signage_pub_bg_cat');
        $crud->set_subject('Background Category');
        $crud->columns('name', 'enabled');
        $crud->display_as('name', 'Category Name');
        $crud->display_as('enabled', 'Is Publish');
        $this->unset_crud_fields('sort');
        $crud->field_type('enabled', 'dropdown', array('1' => 'Publish', '0' => 'Unpublished'));
        $this->mPageTitle = 'Background Category';
        $this->render_crud();
    }

    public function backgrounditem()
    {

        $crud = $this->generate_crud('tb_signage_pub_bg_items');
        $crud->set_subject('Background Items');
        $crud->columns('name','cat_id', 'bg_img','logo','enabled');
        $crud->display_as('name', 'Item Name');
        $crud->display_as('cat_id', 'Category Type');
        $crud->display_as('bg_img', 'Background Image');
        $crud->display_as('logo', 'Logo Image');
        $crud->display_as('enabled', 'Is Publish');
        $this->unset_crud_fields();
        $crud->set_relation('cat_id', 'tb_signage_pub_bg_cat','name');
        $crud->field_type('enabled', 'dropdown', array('1' => 'Publish', '0' => 'Unpublished'));
        $crud->set_field_upload('bg_img', 'assets/uploads/signpub/backitem/');
        $crud->set_field_upload('logo', 'assets/uploads/signpub/backitem/');
        $this->mPageTitle = 'Background Items';
        $this->render_crud();
    }

    function halfHourTimes()
    {
        $formatter = function ($time) {
            if ($time % 3600 == 0) {
                return date('ga', $time);
            } else {
                return date('g:ia', $time);
            }
        };
        $halfHourSteps = range(0, 47 * 1800, 1800);
        return array_map($formatter, $halfHourSteps);
    }

}