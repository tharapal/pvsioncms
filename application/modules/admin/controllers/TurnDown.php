<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 8/29/2017
 * Time: 9:02 AM
 */

class TurnDown extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_builder');
    }
    public function index(){
        $crud = $this->generate_crud('tb_turndown_service');
        $crud->set_subject('TurnDownService');
        $crud->columns('name','message', 'img', 'enabled');
        $this->unset_crud_fields('sort');
        $crud->field_type('enabled','dropdown',array('1' => 'Publish', '0' => 'Unpublished'));
        $crud->set_field_upload('image','assets/uploads/turndown');
        $this->mPageTitle = 'TurnDown Service';
        $this->render_crud();
    }

}