<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 8/29/2017
 * Time: 9:02 AM
 */

class SignageMeeting extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_builder');
    }

    public function index()
    {
        $crud = $this->generate_crud('tb_signage_meetingroom');
        $crud->set_subject('SignageMeeting');
        $crud->columns('name', 'ip_address', 'img', 'logo', 'enabled');
        $this->unset_crud_fields('sort');
        $crud->field_type('enabled', 'dropdown', array('1' => 'Publish', '0' => 'Unpublished'));
        $crud->set_field_upload('img', 'assets/uploads/sgmeeting');
        $crud->set_field_upload('logo', 'assets/uploads/sgmeetinglogo');
        $this->mPageTitle = 'Signage Meeting Room';
        $this->render_crud();
    }

    public function sgnevents()
    {
        $timearray = $this->halfHourTimes();
        //var_dump($timearray);
        $crud = $this->generate_crud('tb_signage_meetingroom_events');
        $crud->set_subject('SignageMeeting Event');
        $crud->columns('name', 'meetingroom_id', 'desc_en', 'logo', 'st_date', 'st_time', 'ed_time', 'enabled');
        $this->unset_crud_fields('sort');
        $crud->display_as('meetingroom_id', 'Meeting Room');
        $crud->display_as('desc_en', 'Description');
        $crud->display_as('logo', 'Log Image');
        $crud->display_as('st_date', 'Start Date');
        $crud->display_as('st_time', 'Start Time');
        $crud->display_as('ed_time', 'End Time');
        $crud->display_as('enabled', 'Is Publish');
        $crud->set_relation('meetingroom_id','tb_signage_meetingroom','name');
        $crud->field_type('enabled', 'dropdown', array('1' => 'Publish', '0' => 'Unpublished'));
        $crud->field_type('st_time', 'dropdown', $timearray);
        $crud->field_type('ed_time', 'dropdown', $timearray);
        $crud->set_field_upload('img', 'assets/uploads/sgmeeting');
        $crud->set_field_upload('logo', 'assets/uploads/sgmeetinglogo');
        $this->mPageTitle = 'Signage Meeting Room';
        $this->render_crud();
    }

    function halfHourTimes()
    {
        $formatter = function ($time) {
            if ($time % 3600 == 0) {
                return date('ga', $time);
            } else {
                return date('g:ia', $time);
            }
        };
        $halfHourSteps = range(0, 47 * 1800, 1800);
        return array_map($formatter, $halfHourSteps);
    }

}