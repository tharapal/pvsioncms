<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 8/29/2017
 * Time: 9:02 AM
 */

class Activity extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_builder');
    }
    public function index(){
        $crud = $this->generate_crud('tb_activity');
        $crud->set_subject('Daily Activities');
        $this->unset_crud_fields('icon','sort','rainy_day');
        $crud->columns('name','price', 'day_id','timing','venue','img', 'enabled','desc_en');
        $crud->field_type('day_id','dropdown',
            array('1' => 'Monday', '2' => 'Tuesday','3' => 'Wednesday' , '4' => 'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday'));
        $crud->display_as('day_id','Day');
        $crud->display_as('desc_en','Description');
        $crud->display_as('img','Upload Image');
        $crud->display_as('enabled','Is Pulish');
        $crud->set_field_upload('img','assets/uploads/activity');
        $crud->field_type('enabled','dropdown',array('1' => 'Publish', '0' => 'Unpublished'));
        $this->mPageTitle = 'Daily Activities';
        $this->render_crud();
    }

}