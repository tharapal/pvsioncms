<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 8/25/2017
 * Time: 2:28 PM
 */

class Accomodation extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_builder');
    }

    // Frontend User CRUD
    public function index()
    {
        $crud = $this->generate_crud('tb_accommodation');
        $crud->columns('name', 'title_en', 'desc_en', 'img', 'enabled');
       // $crud->field_type('enabled','true_false');
        $crud->field_type('enabled','dropdown',array('1' => 'Publish', '0' => 'Unpublished'));
        $this->unset_crud_fields('key_info','a_name_ru','a_name_ge','key_info_ru','key_info_ge','title_ru','title_ge','desc_ru','desc_ge','sort');
        $crud->display_as('name','Name');
        $crud->display_as('title_en','Title');
        $crud->display_as('desc_en','Description');
        $crud->display_as('img','Image Upload');
        $crud->display_as('enabled','Is Publish');
        $crud->set_field_upload('img','assets/uploads/accomodation');
        $this->mPageTitle = 'Accommodation';
        $this->render_crud();
    }
}