<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| CI Bootstrap 3 Configuration
| -------------------------------------------------------------------------
| This file lets you define default values to be passed into views 
| when calling MY_Controller's render() function. 
| 
| See example and detailed explanation from:
| 	/application/config/ci_bootstrap_example.php
*/

$config['ci_bootstrap'] = array(

    // Site name
    'site_name' => 'Palvision-Hotel CMS',

    // Default page title prefix
    'page_title_prefix' => '',

    // Default page title
    'page_title' => '',

    // Default meta data
    'meta_data' => array(
        'author' => '',
        'description' => '',
        'keywords' => ''
    ),

    // Default scripts to embed at page head or end
    'scripts' => array(
        'head' => array(
            'assets/dist/admin/adminlte.min.js',
            'assets/dist/admin/lib.min.js',
            'assets/dist/admin/app.min.js',

        ),
        'foot' => array(),
    ),

    // Default stylesheets to embed at page head
    'stylesheets' => array(
        'screen' => array(
            'https://fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=latin,cyrillic-ext',
            'https://fonts.googleapis.com/icon?family=Material+Icons',
            'assets/dist/admin/adminlte.min.css',
            'assets/dist/admin/lib.min.css',
            'assets/dist/admin/app.min.css',
            'assets/dist/admin/custom.css'
        )
    ),

    // Default CSS class for <body> tag
    'body_class' => '',

    // Multilingual settings
    'languages' => array(),

    // Menu items
    'menu' => array(
        'home' => array(
            'name' => 'Home',
            'url' => '',
            'icon' => 'fa fa-home',
        ),
        'accomodation' => array(
            'name' => 'Accomodation',
            'url' => 'accomodation',
            'icon' => 'fa fa-bed',
        ),
        'azdirectory' => array(
            'name' => 'AZDirectory',
            'url' => 'azdirectory',
            'icon' => 'fa fa-bed',
            'children' => array(
                'Directory' => 'azdirectory',
                'Directory Categoty' => 'azdirectory/dircategory',

            )
        ),
        'banner' => array(
            'name' => 'Banner',
            'url' => 'banner',
            'icon' => 'fa fa-university',
        ),'DeviceManager' => array(
            'name' => 'DeviceManager',
            'url' => 'devicemanager',
            'icon' => 'fa fa-university',
        ),
        'echeck' => array(
            'name' => 'ECheckin',
            'url' => 'echeck',
            'icon' => 'fa fa-check-circle',
        ),
        'facility' => array(
            'name' => 'Facility',
            'url' => 'facility',
            'icon' => 'fa fa-globe',
        ), 'activity' => array(
            'name' => 'Daily Activity',
            'url' => 'activity',
            'icon' => 'fa fa-arrow-down ',
        ), 'turnDown' => array(
            'name' => 'Turn Down Services',
            'url' => 'turnDown',
            'icon' => 'fa fa-arrow-down ',
        ), 'stbox' => array(
            'name' => 'STBox',
            'url' => 'stbox',
            'icon' => 'fa fa-arrow-down ',
        ), 'SignageMeeting' => array(
            'name' => 'Signage Meeting Room',
            'url' => 'signagemeeting',
            'icon' => 'fa fa-address-book ',
            'children' => array(
                'Meeting Room' => 'signagemeeting',
                'Events' => 'signagemeeting/sgnevents'
            )
        ), 'SignagePublic' => array(
            'name' => 'Public Signage',
            'url' => 'signagepublic',
            'icon' => 'fa fa-address-book ',
            'children' => array(
                'Events' => 'signagepublic',
                'Promotions' => 'signagepublic/promotions',
                'Exchange Rate' => 'signagepublic/exchangerate',
                'World Clock' => 'signagepublic/worldclock',
                'World Radio' => 'signagepublic/worldradio',
                'Background Category' => 'signagepublic/backgroundcat',
                'Background Items' => 'signagepublic/backgrounditem',
            )
        ), 'PhotoGallery' => array(
            'name' => 'Photo Gallery',
            'url' => 'photogallery',
            'icon' => 'fa fa-camera',
            'children' => array(
                'Gallery' => 'photogallery',
                'Category' => 'photogallery/category'
            )
        ), 'aboutus' => array(
            'name' => 'About Us',
            'url' => 'AboutUs',
            'icon' => 'fa fa-camera',

        ),
        'user' => array(
            'name' => 'Users',
            'url' => 'user',
            'icon' => 'fa fa-users',
            'children' => array(
                'List' => 'user',
                'Create' => 'user/create',
                'User Groups' => 'user/group',
            )
        ),
        'panel' => array(
            'name' => 'Admin Panel',
            'url' => 'panel',
            'icon' => 'fa fa-cog',
            'children' => array(
                'Admin Users' => 'panel/admin_user',
                'Create Admin User' => 'panel/admin_user_create',
                'Admin User Groups' => 'panel/admin_user_group',
            )
        ),
        'util' => array(
            'name' => 'Utilities',
            'url' => 'util',
            'icon' => 'fa fa-cogs',
            'children' => array(
                'Database Versions' => 'util/list_db',
            )
        ),
        'logout' => array(
            'name' => 'Sign Out',
            'url' => 'panel/logout',
            'icon' => 'fa fa-sign-out',
        )
    ),

    // Login page
    'login_url' => 'admin/login',

    // Restricted pages
    'page_auth' => array(
        'user/create' => array('webmaster', 'admin', 'manager'),
        'user/group' => array('webmaster', 'admin', 'manager'),
        'panel' => array('webmaster'),
        'panel/admin_user' => array('webmaster'),
        'panel/admin_user_create' => array('webmaster'),
        'panel/admin_user_group' => array('webmaster'),
        'util' => array('webmaster'),
        'util/list_db' => array('webmaster'),
        'util/backup_db' => array('webmaster'),
        'util/restore_db' => array('webmaster'),
        'util/remove_db' => array('webmaster'),
    ),

    // AdminLTE settings
    'adminlte' => array(
        'body_class' => array(
            'webmaster' => 'skin-red',
            'admin' => 'skin-purple',
            'manager' => 'skin-black',
            'staff' => 'skin-blue',
        )
    ),

    // Useful links to display at bottom of sidemenu
    'useful_links' => array(
        array(
            'auth' => array('webmaster', 'admin', 'manager', 'staff'),
            'name' => 'Frontend Website',
            'url' => '',
            'target' => '_blank',
            'color' => 'text-aqua'
        ),
        array(
            'auth' => array('webmaster', 'admin'),
            'name' => 'API Site',
            'url' => 'api',
            'target' => '_blank',
            'color' => 'text-orange'
        ),

    ),

    // Debug tools
    'debug' => array(
        'view_data' => FALSE,
        'profiler' => FALSE
    ),
);

/*
| -------------------------------------------------------------------------
| Override values from /application/config/config.php
| -------------------------------------------------------------------------
*/
$config['sess_cookie_name'] = 'ci_session_admin';