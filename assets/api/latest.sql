create table device_manager
(
	id int auto_increment	primary key,
	betterystate int null,
	message varchar(150) null,
	ssid varchar(10) null,
	wifilevel int null,
	wifimessage varchar(150) null,
	httpresponse int null,
	created_at timestamp default CURRENT_TIMESTAMP not null
)
;

